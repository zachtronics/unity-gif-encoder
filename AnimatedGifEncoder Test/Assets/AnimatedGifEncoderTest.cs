﻿using GifEncoder;
using System;
using System.IO;
using UnityEngine;

public class AnimatedGifEncoderTest : MonoBehaviour
{
    public Camera renderCamera;
    public Transform testCube;

    private RenderTexture renderTexture;
    private AnimatedGifEncoder gifEncoder;
    private float cameraAngle;
    
    public void Start()
    {
        // Configure the camera to render manually into the render texture:
        this.renderTexture = new RenderTexture(320, 240, 24);
        this.renderCamera.enabled = false;
        this.renderCamera.targetTexture = this.renderTexture;

        // Create a GIF encoder:
        this.gifEncoder = new AnimatedGifEncoder(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "output.gif"));
        this.gifEncoder.SetDelay(1000 / 30);
    }

    public void Update()
    {
        // Force the render camera, which we disabled earlier, to render a frame:
        this.renderCamera.Render();

        // Copy the render texture data into a temporary texture:
        RenderTexture.active = this.renderTexture;
        Texture2D frameTexture = new Texture2D(this.renderTexture.width, this.renderTexture.height, TextureFormat.RGB24, false);
        frameTexture.ReadPixels(new Rect(0, 0, this.renderTexture.width, this.renderTexture.height), 0, 0);

        // Add the current frame to the GIF:
        this.gifEncoder.AddFrame(frameTexture);

        // Destroy the temporary texture:
        UnityEngine.Object.Destroy(frameTexture);

        // After we've rotated the cube exactly once we should finalize the GIF so that it loops seamlessly:
        this.cameraAngle += 5;
        this.testCube.eulerAngles = new Vector3(0, this.cameraAngle, 0);
        if (Mathf.Approximately(this.cameraAngle, 360))
        {
            this.gifEncoder.Finish();
            this.Quit();
        }
    }

    private void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
